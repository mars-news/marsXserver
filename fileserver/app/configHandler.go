package app

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
	"MarsXserver/tcpserver"
	"MarsXserver/httpserver"
)



type BaseFileServerConfigData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`

	StaticFilePath string	`xml:"db-res-path"`

	TConfig tcpserver.TcpConfigData	`xml:"tconfig"`
	HConfig httpserver.HttpConfigData `xml:"hconfig"`
}







