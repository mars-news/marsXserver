package app

import (
	"MarsXserver/common"
	"MarsXserver/httpserver"
	"MarsXserver/hybridServer"
	"MarsXserver/orm"
)

const (
	Dialer_Type_DB      string = "db"
	Dialer_Type_RED		string = "red"
)

var (
	All_Dialers = []string{Dialer_Type_DB, Dialer_Type_RED}
)


type BaseFileServer struct{
	hybridServer.HybridBaseServer

	Sid	int
	Sname string
	Config *FileServerConfigData

}

func (this *BaseFileServer)InitBaseFileServer(config *BaseFileServerConfigData) (*BaseFileServer, error){

	config, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	//fileServer := &BaseFileServer{
	//	Sid: 	config.Sid,
	//	Sname:  config.Name,
	//	Config: config,
	//}

	this.Sid = config.Sid
	this.Sname = config.Name
	this.Config = config

	this.InitBaseServer(config.Name, this, &config.TConfig, &config.HConfig, nil, nil)

	this.Orm.RegisterSendFunc(this.OrmSendFunc)
	this.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	this.TServer.SetLegalDialerNames(All_Dialers)

	this.HServer.RegisterAuthFunc(httpserver.Auth)

	return this, nil
}

func (this *BaseFileServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	this.OrmSendFuncByType(Dialer_Type_DB, expr, retCh)
}




func (this *BaseFileServer) Run(blockMode bool) error{

	return this.RunBaseServer(blockMode)

}
















