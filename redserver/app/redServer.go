package app

import (
	"MarsXserver/orm"
	"MarsXserver/common"
	"MarsXserver/hybridServer"
	"MarsXserver/redmodule"
)



type RedServer struct{
	hybridServer.HybridBaseServer

	Sid	int
	Sname string
	Config *RedServerConfigData

}



func NewRedServer(configName string) (*RedServer, error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	redServer := &RedServer{
		Sid: 	config_.Sid,
		Sname:  config_.Name,
		Config: config_,
	}

	redServer.InitBaseServer(config_.Name, redServer, &config_.TConfig, nil, nil, &config_.RConfig)

	redServer.Orm.RegisterSendFunc(redServer.OrmSendFunc)
	redServer.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	redServer.AddGcInfoTask("gc", common.Red_GC_Interval, redServer.GC)

	return redServer, nil
}



func (this *RedServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	//this.OrmSendFuncByType(Dialer_Type_DB, expr, retCh)

	common.ErrorLog("red server has no orm send function")
	close(retCh)
}

func (this *RedServer) RedSendFunc(req *redmodule.RedDataRequest, retCh chan interface{}){

	common.ErrorLog("red server has no red send function")
	close(retCh)

}


func (this *RedServer) Run(blockMode bool) error{

	return this.RunBaseServer(blockMode)
}



func (this *RedServer) GC(){

	this.Red.TimeGCAll()

}





















