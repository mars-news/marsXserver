package redserver

import (
	"MarsXserver/redserver/app"
	"MarsXserver/tcpserver"
	"reflect"
	"MarsXserver/redmodule"
	"MarsXserver/redserver/objHandler"
)


func initRegisterTcpHandlers(sServer *app.RedServer){
	tcpserver.RegisterHandler(
		int(redmodule.MessageId_Data_Op),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSRedDataOpRequestHandler{},
			ReqMsgType:   reflect.TypeOf(redmodule.RedDataRequest{}),
			RspMsgType:	reflect.TypeOf(redmodule.RedDataResponse{}),
			IsByPassSecondRspMsgDecoder: true,			//!important receive buffer bytes
		})

}


func initRegisterHttpHandlers(aServer *app.RedServer){

	HttpHandlers := make(map[string]interface{})

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.RedServer){



}

func InitRegisterHandlers(aServer *app.RedServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

