package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/redmodule"
)

type SSRedDataOpRequestHandler struct{

}


func (handler *SSRedDataOpRequestHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	reqHeader := header.(*common.ObjRequestHeader)

	req, ok := body.(*redmodule.RedDataRequest)
	if ok == false{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("body is not expr request")
	}

	var res interface{}

	switch req.Op {
	case redmodule.RedDataOpRead:

		resArr := make([]interface{}, len(req.StructNames))

		for ii := 0; ii < len(req.StructNames); ii++{
			resItem := connector.Server.Red.ReadHashServer(req.StructNames[ii], req.IdValsArr[ii])
			if resItem == nil{
				connector.SendObjErrorResponse(reqHeader, -1)
				return common.ErrorLog("red read server failed", req.StructName)
			}
			resArr[ii] = resItem
		}

		res = resArr
	case redmodule.RedDataOpSave:
		savedId, err := connector.Server.Red.SaveHashServer(req.StructName, req.IdVals, req.FieldVals, true)
		if err != nil{
			connector.SendObjErrorResponse(reqHeader, -1)
			return common.ErrorLog("red save server failed", req.StructName, err)
		}
		res = savedId
	case redmodule.RedDataOpUpdate:
		savedId, err :=connector.Server.Red.SaveHashServer(req.StructName, req.IdVals, req.FieldVals, false)
		if err != nil{
			connector.SendObjErrorResponse(reqHeader, -1)
			return common.ErrorLog("red update server failed", req.StructName)
		}
		res = savedId
	case redmodule.RedDataOpDel:
		if err := connector.Server.Red.DeleteHashServer(req.StructName, req.IdVals); err != nil{
			res = int64(-1)
		}else{
			res = int64(1)
		}
	case redmodule.RedDataOpCnt:
		cnt, err := connector.Server.Red.GetCountServer(req.StructName, req.IdVals, req.CntIdx)
		if err != nil{
			connector.SendObjErrorResponse(reqHeader, -1)
			return common.ErrorLog("red cnt server failed", req.StructName)
		}
		res = cnt
	default:
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("red op err", req.Op)
	}

	rsp := &redmodule.RedDataResponse{
		Op: req.Op,
		Data: res,
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(redmodule.MessageId_Data_Op))
		return err
	}

	return nil

}
























