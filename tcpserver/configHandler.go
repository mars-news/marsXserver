package tcpserver

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
)


const (
	serverListConfigFileName = "servers"
)

var (
	serverListConfig *ServerListConfigData
)

type ServerListConfigData struct{
	ServerList TcpServerItems       `xml:"servers"`
}

type TcpConfigData struct{

	Sid       int			`xml:"sid"`
	Dialers	TcpDialerItems		`xml:"dialers"`
}

type TcpDialerItems struct{
	Dialer	[]int		`xml:"dialer"`
}


type TcpServerItems struct{
	Servers []TcpServerConfigItem `xml:"server"`
}


type TcpServerConfigItem struct{
	Sid   int		`xml:"sid"`
	SType string	`xml:"type"`
	Ip    string	`xml:"ip"`
	Port  int	`xml:"port"`
}

func LoadServersConfig(name string) (configData *ServerListConfigData, err error){

	configData = &ServerListConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	return configData, nil
}

func LoadConfig(name string) (configData *TcpConfigData, err error){

	configData = &TcpConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	return configData, nil
}


func (this *TcpConfigData) GetCurrServerItem() (item *TcpServerConfigItem, err error){

	serverItem, err := GetServerItemById(this.Sid)
	if err != nil{
		common.ErrorLog("curr tcpserver sid is not correct", this.Sid)
		return nil, err
	}
	return serverItem, nil

}

func GetServerItemById(sid int) (item *TcpServerConfigItem, err error){

	for _, svr := range serverListConfig.ServerList.Servers {
		if svr.Sid == sid{

			return &svr, nil
		}

	}

	return nil, common.ErrorLog("tcpserver sid is not correct", sid)
}


func init(){
	var err error
	if serverListConfig, err = LoadServersConfig(serverListConfigFileName); err != nil{
		common.ErrorLog("server list config file load failed")
	}
}






















