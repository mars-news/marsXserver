package tcpserver

import (
	"reflect"
	"fmt"
	"errors"
	"MarsXserver/common"
)

type MessageProtoType int


const(
	_ MessageProtoType = iota
	MessageProtoType_Proto
	MessageProtoType_ObjRequest
	MessageProtoType_ObjResponse
	MessageProtoType_FileRequest
	MessageProtoType_FileResponse
)



func init(){
	RegisterHandler(
		int(common.MessageId_ObjRequestMessageId),
		&HandlerItem{
			MsgProtoType: MessageProtoType_ObjRequest,
		})

	RegisterHandler(
		int(common.MessageId_ObjReponseMessageId),
		&HandlerItem{
			MsgProtoType: MessageProtoType_ObjResponse,
		})
}


type MsgHandler interface {
	Handle(connector *Connector, header interface{}, body interface{}) error
}

type MsgGetIdFunc func(msg interface{}) (msgId int, err error)

type MsgGetSeqFunc func(msg interface{}) (seq int64, err error)

type MsgSetSeqFunc func(msg interface{}, seq int64) error

type MsgSecondDecoder func(buf *common.MBuffer) (interface{}, error)

type HandlerItem struct{

	MsgId int

	MsgProtoType MessageProtoType

	ReqMsgType reflect.Type
	RspMsgType reflect.Type

	Handler MsgHandler

	//used by root msg
	MsgGetIdFunc MsgGetIdFunc

	MsgGetSeqFunc MsgGetSeqFunc

	MsgSetSeqFunc MsgSetSeqFunc
	//used by

	IsByPassSecondRspMsgDecoder bool

}

var handlerMap = make(map[int]*HandlerItem)
var handlerReverseMap = make(map[reflect.Type]int)


func getMsgRegItemById(msgid int) (*HandlerItem, error){

	msgItem, check := handlerMap[msgid]
	if !check{
		err := errors.New(fmt.Sprintf("no handler : %v", msgid))
		return nil, err
	}
	return msgItem, nil

}


func getMsgIdByMsg(msg interface{}) (int, error){

	obj := reflect.ValueOf(msg)
	if obj.Kind() != reflect.Ptr{
		return -1, common.ErrorLog("send msg is not pointer struct or nil")
	}

	ind := reflect.Indirect(obj)

	msgId, ok := handlerReverseMap[ind.Type()]
	if ok == false{
		return -1, common.ErrorLog("no such type", ind.Type().Name())
	}

	return msgId, nil

}


func RegisterHandler(msgId int, handlerItem* HandlerItem) {

	if _, found := handlerMap[msgId]; found == true{
		common.ErrorLog(fmt.Sprintf("msg id is registered:%v", msgId))
		return
	}
	handlerItem.MsgId = msgId
	handlerMap[msgId] = handlerItem
	if handlerItem.RspMsgType != nil{
		handlerReverseMap[handlerItem.RspMsgType] = msgId
	}
	if handlerItem.ReqMsgType != nil{
		handlerReverseMap[handlerItem.ReqMsgType] = msgId
	}
}


func SetMessageSeq(message interface{}, msgId int,seq int64) error{

	handlerItem, found := handlerMap[msgId]
	if found == false{
		return common.ErrorLog("handler item not found, msgid:", msgId)
	}

	handlerItem.MsgSetSeqFunc(message, seq)
	return nil
}



func DispatchProtoMessage(connector *Connector, message interface{}, handlerItem *HandlerItem) error{

	if handlerItem.MsgGetIdFunc == nil{
		return common.ErrorLog("msg get id func nil, id:", handlerItem.MsgId)
	}

	if handlerItem.MsgGetSeqFunc == nil{
		return common.ErrorLog("msg get seq func nil id:", handlerItem.MsgId)
	}

	seq, err := handlerItem.MsgGetSeqFunc(message)
	retCb, ok := connector.Server.callbacks.Get(seq).(*tcpCallback)
	if ok == true{
		retCb.ret <- message
		connector.Server.callbacks.Delete(seq)
		return nil
	}


	subMsgId,err := handlerItem.MsgGetIdFunc(message)
	if err != nil{
		return common.ErrorLog("get sub msg id null, ", handlerItem.MsgId)
	}

	subHandlerItem, ok := handlerMap[subMsgId]
	if ok != true || subHandlerItem == nil{
		return common.ErrorLog("dispatch cannot find handler by id:", subMsgId)
	}

	if subHandlerItem.Handler == nil{
		return common.ErrorLog("sub msg handler nil, id:", handlerItem.MsgId)
	}

	if err := subHandlerItem.Handler.Handle(connector, nil, message); err != nil{
		return errors.New(fmt.Sprintf("handle failed: %v", handlerItem.MsgId))
	}


	return nil
}




func (this *Connector) ObjRequestPackageDispatch(buf *common.MBuffer, parentHandlerItem *HandlerItem) error{

	reqHeader := &common.ObjRequestHeader{}

	if err := common.DecodeObjectPacket(buf, reqHeader); err != nil{
		return common.ErrorLog("decode header failed")
	}


	handlerItem, err := getMsgRegItemById(reqHeader.Id)
	if err != nil{
		return common.ErrorLog(" get second msg item faied parent", reqHeader.Id, " sec id:", reqHeader.Id)
	}


	var msg interface{}

	msg = reflect.New(handlerItem.ReqMsgType).Interface()
	if err = common.DecodeObjectPacket(buf, msg); err != nil{
		return common.ErrorLog("decode body failed:", *reqHeader)
	}

	if err := handlerItem.Handler.Handle(this, reqHeader, msg); err != nil{
		return common.ErrorLog(fmt.Sprintf("handle failed: %v", handlerItem.MsgId))
	}

	return nil

}

func (this *Connector) ObjResponsePackageDispatch(buf *common.MBuffer, parentHandlerItem *HandlerItem) error{

	rspHeader := &common.ObjResponseHeader{}

	if err := common.DecodeObjectPacket(buf, rspHeader); err != nil{
		return common.ErrorLog("decode header failed")
	}


	if !this.Server.callbacks.Contains(rspHeader.Seq){
		return common.ErrorLog("not rsp with this seq:", *rspHeader)
	}

	cb, ok := this.Server.callbacks.Get(rspHeader.Seq).(*tcpCallback)
	if ok == false{
		return common.ErrorLog("get rsp cb error")
	}

	handlerItem, err := getMsgRegItemById(rspHeader.Id)
	if err != nil{
		return common.ErrorLog(" get second msg item faied parent", rspHeader.Id, " sec id:", rspHeader.Id)
	}


	defer func(){

		this.Server.callbacks.Delete(rspHeader.Seq)

	}()

	var msg interface{}

	if rspHeader.Err != 0{

		common.InfoLog("response is error", *rspHeader)

		close(cb.ret)

		return nil
	}


	if handlerItem.IsByPassSecondRspMsgDecoder{

		cb.ret <- common.NewBufferFromBuffer(buf)
		return nil
	}else{

		msg = reflect.New(handlerItem.RspMsgType).Interface()
		if err = common.DecodeObjectPacket(buf, msg); err != nil{
			return common.ErrorLog("decode body failed:", *rspHeader)
		}

	}

	cb.ret <- msg

	return nil

}
















