package app

import (
	"MarsXserver/common"
	"MarsXserver/hybridServer"
)



type DbServer struct{
	hybridServer.HybridBaseServer

	Name string
	Config *DBServerConfigData

}



func NewDbServer(configName string) (dbsvr *DbServer, err error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	dbserver := &DbServer{
		Name:   config_.Name,
		Config: config_,
	}


	dbserver.InitBaseServer(config_.Name, dbserver, &config_.TConfig, &config_.HConfig, &config_.OrmConfig, nil)

	return dbserver, nil
}




func (this *DbServer) Run(blockMode bool) error{

	return this.RunBaseServer(blockMode)
}

























