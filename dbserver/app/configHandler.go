package app

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
	"MarsXserver/tcpserver"
	"MarsXserver/httpserver"
	"MarsXserver/orm"
)



type DBServerConfigData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`


	TConfig tcpserver.TcpConfigData	`xml:"tconfig"`
	HConfig httpserver.HttpConfigData `xml:"hconfig"`
	OrmConfig orm.OrmConfigData `xml:"orm-config"`
}


func LoadConfig(name string) (configData *DBServerConfigData, err error){

	configData = &DBServerConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read Config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	return configData, nil
}

























