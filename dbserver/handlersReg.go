package dbserver

import (
	"MarsXserver/dbserver/app"
	"MarsXserver/dbserver/objHandler"
	"MarsXserver/tcpserver"
	"reflect"
	"MarsXserver/orm"
)

func initRegisterTcpHandlers(aServer *app.DbServer){

	tcpserver.RegisterHandler(
		int(orm.MessageId_DB_Expr),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSDBExprRequestHandler{},
			ReqMsgType:   reflect.TypeOf(orm.DBExprRequest{}),
			RspMsgType:	reflect.TypeOf(orm.DBExprResponse{}),
			IsByPassSecondRspMsgDecoder: true,			//!important receive buffer bytes

		})
}


func initRegisterFileHandlers(aServer *app.DbServer){


}



func InitRegisterHandlers(aServer *app.DbServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
}































