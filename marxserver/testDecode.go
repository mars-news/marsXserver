package main

import (
	"MarsXserver/common"
)

type TestDe struct{

	AA int
	BB string
	CC []int
	DD []byte

}




func testDecode(){

	res, _ := common.EncodeObjectPacket(&TestDe{
		AA:	12,
		BB: 	"aaa",
		CC:	[]int{1, 2, 3},
		DD:	[]byte{4, 5, 6},
	})


	buf := common.NewBufferByBytes(res)

	testObj := &TestDe{}

	common.DecodeObjectPacket(buf, testObj)

	common.InfoLog("obj :", *testObj)
}
