package main

import "MarsXserver/common"

type BaseA struct{
	name string
	fea string

}


func (this *BaseA) echo(){
	common.InfoLog("base")
}

type ChildA struct{

	BaseA
	name string
}

func (this *ChildA) echo(){
	common.InfoLog("child")
}

func testHier(){

	child := new(ChildA)
	child.fea = "aaa"
	common.InfoLog(*child)


}