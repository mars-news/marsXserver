package main

import (
	"time"
	"MarsXserver/common"
)

func testRef(err *error, res chan interface{}){


	time.AfterFunc(time.Second * 3, func(){
		res <- 123

		*err = common.ErrorLog("aaa")
		close(res)
	})

}


func testRefMain(){
	var err error
	resCh := make(chan interface{})

	go testRef(&err, resCh)

	for rr := range resCh{

		common.InfoLog(rr.(int))

	}

	common.InfoLog(err)

	return



}




