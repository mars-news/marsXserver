[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

## A Golang Web Framework

This is a server-side web framework written in Go, containing multiple underlying modules.
The web server written with it can be found at https://gitlab.com/mars-news/NewsServer

1. TCP communication module (in the tcpserver folder), which handles connection requests asynchronously and processes requests from different clients in multiple threads. Communication uses the protobuf protocol.
2. HTTP communication module (in the httpServer folder), implementing Post and Get request handling, Session control, JWT authorization management, web page Template rendering, and Websocket communication.
3. File service module (in the fileserver folder), implementing file upload and download functionality based on HTTP.
4. ORM functionality based on MySQL (in the orm folder). Database tables can be defined through annotations, and tables are automatically created when the program starts.
Table definition method:
```
type User struct {
	Id int32    `orm:"auto"`
	Name string `orm:"size(20)"`
	Teacher *TestTeacher `orm:"rel,default"`
}

type TestTeacher struct {
	Id int32    `orm:"auto"`
	Name string `orm:"size(20)"`
}
```
5. NoSQL functionality based on Redis (in the orm folder), implementing encoding and decoding for communication with the Redis server, and reading and writing of complex models.
6. Bridge module (in the bridge folder), for communication between different modules, such as TCP and HTTP, when both are present in the server.



[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555


[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-url]: https://www.linkedin.com/in/shaokun-feng/