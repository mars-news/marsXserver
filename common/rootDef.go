package common

import "time"

const (
	MaxUint64 = ^uint64(0)
	MinUint64 = 0
	MaxInt64 = int(MaxUint64 >> 1)
	MinInt64 = -MaxInt64 - 1

	Epsilon = 0.0000001


)


var (  //note: must defined per project

	DefaultConfPath = "xxxx"
	DefaultCookieName = "xxxxxxxxxx"
	DefaultProtoBinPath = "xxxx"

)

var (
	Red_GC_Interval = time.Minute * 30
)

const (
	Default_Http_Hijack_Max_Wait		time.Duration		= time.Second * 100
	Default_Http_Hijack_Clean_Period	time.Duration		= time.Second * 10
)


const (
	Default_User_Token_Length = 20
)

const(
	SessionKey_UID = "uid"
)

const(
	MessageId_ObjRequestMessageId      = 100
	MessageId_ObjReponseMessageId      = 101

	MessageId_DbObjMessageIdStart      = 200

	MessageId_RedMessageIdStart		   = 300

	MessageId_FileRequest 			   = 400
	MessageId_FileResponse             = 401

	Root_TcpObserverType			   = 500
	Root_HttpObserverType			   = 501
)